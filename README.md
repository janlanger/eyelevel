Installation
=============

Clone repository
----------------

- `git clone https://github.com/janlanger/eyelevel.git`
- `cd eyelevel`

Requirments
-----------

- PHP 5.5+
- Composer
- Node.js
- Npm
- gulp
- bower


Installation
------------

- `make install`

or without make:
 - `composer install` in ./server
 - `npm install && bower install` in ./client

- copy `server/app/config/config.local.neon.sample` to config.local.neon and set your database configuration in there
- create your database (using some other tool)
- run `php server/www/api.php orm:schema-tool:create` to create database tables

Build
-----

- `make build`

or without make:
 - `gulp build` in ./client
