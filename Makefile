SHELL = bash
PUBLIC_REPO = public
OS = $(shell uname)

LOCALPATH = $(CURDIR)



all: install build

merged:
	$(SHELL) buildtools/delete-merged.sh $(PUBLIC_REPO)

api-mock:
	cd $(LOCALPATH)/client && ./node_modules/.bin/api-mock $(LOCALPATH)/api/apiary.apib

install:
	cd $(LOCALPATH)/client && npm install && bower install
	cd $(LOCALPATH)/server && composer install

build:
	cd $(LOCALPATH)/client && ./node_modules/.bin/gulp build
	cd $(LOCALPATH)/server && composer install


include buildtools/Makefile.$(OS)
