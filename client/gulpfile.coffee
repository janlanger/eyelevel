gulp = require 'gulp'
gutil = require 'gulp-util'
runSequence = require 'run-sequence'
browserSync = require 'browser-sync'
g = require('gulp-load-plugins')()



isProd = gutil.env.type is 'prod'

sources =
  less: 'src/**/*.less'
  html: 'src/**/*.html'
  coffee: 'src/**/*.coffee'
  lib: 'bower_components/**/**'

destinations =
  css: '../server/www/'
  html: '../server/www/'
  js: '../server/www/'
  lib: '../server/www/libs/'



gulp.task 'browser-sync', ->
  browserSync.init null,
    open: false
    server:
      baseDir: "../server/www"
    watchOptions:
      debounceDelay: 1000

gulp.task 'style', ->
  gulp
    .src(sources.less)
    .pipe(g.less())
    .pipe(gulp.dest(destinations.css))

gulp.task 'html', ->
  gulp
    .src(sources.html)
    .pipe(gulp.dest(destinations.html))

gulp.task 'lint', ->
  gulp
    .src(sources.coffee)
    .pipe(g.coffeelint())
    .pipe(g.coffeelint.reporter())

gulp.task 'src', ->
  gulp
    .src(sources.coffee)
    .pipe(g.coffee({bare: true}).on('error', gutil.log))
    .pipe(if isProd then g.uglify() else gutil.noop())
    .pipe(gulp.dest(destinations.js))

gulp.task 'lib', ->
  gulp
    .src(sources.lib)
    .pipe(gulp.dest(destinations.lib))

gulp.task 'watch', ->
  gulp.watch sources.less, ['style']
  gulp.watch sources.app, ['lint', 'src', 'html']
  gulp.watch sources.html, ['html']
  gulp.watch sources.coffee, ['src']

  gulp.watch 'dist/**/**', (file) ->
    browserSync.reload(file.path) if file.type is "changed"

gulp.task 'clean', ->
  gulp
    .src(['dist/'], {read: false})
    .pipe(g.clean())

gulp.task 'build', ->
  runSequence 'clean', ['style', 'lint', 'src', 'lib', 'html']

gulp.task 'default', [
  'build'
  'browser-sync'
  'watch'
]