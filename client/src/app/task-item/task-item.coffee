Polymer

  publish:
    task: null
    index: 0

  mode: ''

  closeTask: ->
    @mode = ''
    @fire 'task-close', @index

  editTask: ->
    @toggleMode 'edit'

  addTime: ->
    @toggleMode 'time'

  detailTask: ->
    @toggleMode 'detail'

  saveTask: ->
    @mode = ''
    @fire 'task-save', @index

  saveTime: ->
    @mode = ''
    @fire 'task-time', @index

  toggleMode: (type) ->
    @mode = if @mode == type then '' else type