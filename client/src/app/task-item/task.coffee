class Task

  constructor: (obj = {}) ->
    @id = obj.id or null
    @name = obj.name or ''
    @created = obj.created or null
    @closed = obj.closed or null
    @duration = obj.duration or 0
