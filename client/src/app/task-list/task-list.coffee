BASE_URL = '/api/tasks'
Polymer

  tasks: []

  request:
    type: 'list'
    index: 0
    method: 'GET'
    url: BASE_URL
    body: {}

  ready: ->


  createTask: ->
    if @newTask isnt ''
      @tasks.push new Task({name: @newTask})

      @request =
        type: 'create'
        index: @tasks.length - 1
        method: 'POST'
        url: BASE_URL
        body: JSON.stringify
          name: @newTask

      @newTask = ''

  closeTask: (event, index) ->
    task = @tasks[index]
    @request =
      type: 'close'
      index: index
      method: 'PUT'
      url: "#{BASE_URL}/#{task.id}/close"
      body: {}

  saveTask: (event, index) ->
    task = @tasks[index]
    @request =
      type: 'save'
      index: index
      method: 'PUT'
      url: "#{BASE_URL}/#{task.id}"
      body: JSON.stringify task

  timeTask: (event, index) ->
    task = @tasks[index]
    @request =
      type: 'time'
      index: index
      method: 'PUT'
      url: "#{BASE_URL}/#{task.id}/time"
      body: JSON.stringify
        duration: task.duration

  handleResponse: (event, data) ->
    switch @request.type
      when 'list'
        @tasks = data.response.map (obj) -> new Task(obj)
      when 'close'
        @tasks[@request.index] = data.response
      when 'create'
        @tasks[@request.index] = data.response