<?php


namespace App\ApiModule\Presenters;


use App\BasePresenter;

abstract class ApiPresenter extends BasePresenter
{

	protected function beforeRender()
	{
		parent::beforeRender();
		$this->sendJson(['message' => 'no data']);
	}


} 