<?php


namespace App\ApiModule\Presenters;


use App\Model\TaskFacade;
use Nette\Application\BadRequestException;
use Nette\Utils\Json;
use Nette\Utils\JsonException;

class TasksPresenter extends ApiPresenter
{

	/** @var  TaskFacade @autowire */
	protected $facade;


	public function actionReadAll()
	{

		$data = $this->facade->getTasks();

		$this->sendJson(array_values($data));
	}

	public function actionRead($id)
	{
		if (!$id) {
			throw new BadRequestException();
		}
		$data = $this->facade->getTask($id);
		if (!$data) {
			throw new BadRequestException();
		}

		$this->sendJson($data);
	}

	public function actionCreate()
	{
		try {
			$data = Json::decode($this->getParameter('data', '{}'));
		} catch (JsonException $e) {
			$data = NULL;
		}

		if (!empty($data)) {
			try {
				$entity = $this->facade->createTask($data->name);
			} catch (\InvalidArgumentException $e) {
				throw new BadRequestException();
			}
			$this->sendJson($entity);
		}


	}

	public function actionUpdate($id)
	{
		try {
			$data = Json::decode($this->getParameter('data', '{}'));
		} catch (JsonException $e) {
			$data = NULL;
		}

		if (!empty($data)) {
			try {
				$entity = $this->facade->updateTask($id, $data->name);
			} catch (\InvalidArgumentException $e) {
				throw new BadRequestException();
			}
			$this->sendJson($entity);
		}
	}

} 