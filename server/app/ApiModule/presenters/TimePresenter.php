<?php


namespace App\ApiModule\Presenters;


use App\Model\TaskFacade;
use Nette\Application\BadRequestException;
use Nette\Utils\Json;
use Nette\Utils\JsonException;

class TimePresenter extends ApiPresenter
{
	/** @var TaskFacade @autowire */
	protected $facade;

	public function actionUpdate(array $associations, $data)
	{
		try {
			$data = Json::decode($data);
		} catch (JsonException $e) {
			$data = NULL;
		}

		if(!empty($data) && !empty($associations['tasks'])) {
			try {
				$entity = $this->facade->logTime($associations['tasks'], (int)$data->duration);
			} catch (\InvalidArgumentException $e) {
				throw new BadRequestException();
			}
			$this->sendJson($entity);
		}

	}

} 