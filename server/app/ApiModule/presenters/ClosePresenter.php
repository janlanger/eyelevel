<?php


namespace App\ApiModule\Presenters;


use App\Model\TaskFacade;
use Nette\Application\BadRequestException;
use Nette\Utils\Json;
use Nette\Utils\JsonException;

class ClosePresenter extends ApiPresenter
{
	/** @var TaskFacade @autowire */
	protected $facade;

	public function actionUpdate(array $associations)
	{

		if(!empty($associations['tasks'])) {
			try {
				$entity = $this->facade->closeTask($associations['tasks']);
			} catch (\InvalidArgumentException $e) {
				throw new BadRequestException();
			}
			$this->sendJson($entity);
		}

	}

} 