<?php

namespace App;

use AdamStipak\RestRoute;
use Nette,
	Nette\Application\Routers\RouteList,
	Nette\Application\Routers\Route,
	Nette\Application\Routers\SimpleRouter;


/**
 * Router factory.
 */
class RouterFactory
{

	/**
	 * @return \Nette\Application\IRouter
	 */
	public function createRouter()
	{
		$router = new RouteList();
		$router[] = $rest = new RestRoute('Api', 'json', TRUE);
		$router[] = new Route('<presenter>/<action>[/<id>]', 'Default:default');
		return $router;
	}

}
