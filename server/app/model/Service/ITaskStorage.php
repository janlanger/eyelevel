<?php


namespace App\Model;


use App\Model\Task\Task;

interface ITaskStorage
{

	/**
	 * Get all tasks
	 *
	 * @return Task[]
	 */
	public function getAll();

	/**
	 * Get one task
	 * @param $id int
	 * @return Task|NULL
	 */
	public function get($id);

	/**
	 * @param $entity Task
	 * @return Task
	 */
	public function save($entity);
}