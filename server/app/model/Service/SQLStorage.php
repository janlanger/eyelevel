<?php


namespace App\Model;


use App\Model\Task\Task;
use Kdyby\Doctrine\EntityManager;

class SQLStorage implements ITaskStorage
{
	/** @var EntityManager */
	private $em;

	/** @var \Kdyby\Doctrine\EntityDao */
	private $taskDao;

	function __construct(EntityManager $em)
	{
		$this->em = $em;
		$this->taskDao = $em->getDao(Task::class);
	}


	public function getAll()
	{
		return $this->taskDao->findAll();
	}

	public function get($id)
	{
		return $this->taskDao->find($id);
	}

	public function save($entity)
	{
		return $this->taskDao->save($entity);
	}
}