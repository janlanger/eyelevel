<?php


namespace App\Model;


use App\Model\Task\Task;

class TaskFacade
{
	/** @var ITaskStorage */
	private $storage;

	public function __construct(ITaskStorage $storage)
	{
		$this->storage = $storage;
	}

	/**
	 * @return Task[]
	 */
	public function getTasks()
	{
		return $this->storage->getAll();
	}

	/**
	 * @param int $id
	 * @return Task|NULL
	 */
	public function getTask($id)
	{
		return $this->storage->get($id);
	}

	/**
	 * @param string $title
	 * @return Task
	 */
	public function createTask($title)
	{
		$entity = new Task();
		$entity->name = $title;
		$this->storage->save($entity);

		return $entity;
	}

	/**
	 * @param string $id
	 * @param string $title
	 * @return Task|NULL
	 * @throws \InvalidArgumentException if task does not exists
	 */
	public function updateTask($id, $title)
	{
		$entity = $this->getTask($id);
		if (!$entity) {
			throw new \InvalidArgumentException('Task not found.');
		}

		$entity->name = $title;

		$this->storage->save($entity);

		return $entity;
	}

	/**
	 * @param int $id
	 * @param int $time
	 * @return Task|NULL
	 * @throws \InvalidArgumentException if task does not exists
	 */
	public function logTime($id, $time)
	{
		$entity = $this->getTask($id);

		if (!$entity) {
			throw new \InvalidArgumentException('Task not found.');
		}

		$entity->duration = $time;

		$this->storage->save($entity);

		return $entity;
	}

	/**
	 * @param int $id
	 * @return Task|NULL
	 *
	 * @throws \InvalidArgumentException if task does not exists
	 */
	public function closeTask($id)
	{
		$entity = $this->getTask($id);

		if(!$entity) {
			throw new \InvalidArgumentException('Task not found.');
		}

		$entity->closed = new \DateTime();

		$this->storage->save($entity);
		return $entity;
	}



}