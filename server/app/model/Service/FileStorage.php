<?php


namespace App\Model;


class FileStorage implements ITaskStorage
{

	private $data = FALSE;

	private $filepath;

	public function __construct($filepath)
	{
		$this->filepath = $filepath;
	}


	public function getAll()
	{
		$this->load();

		return $this->data;
	}

	public function get($id)
	{
		$this->load();

		return !empty($this->data[$id]) ? $this->data[$id] : NULL;
	}

	public function save($entity)
	{
		$this->load();
		if(!$entity->getId()) {
			$entity->id = empty($this->data) ? 1 : max(array_keys($this->data)) +1;
		}

		$this->data[$entity->getId()] = $entity;

		//save
		$data = serialize($this->data);

		file_put_contents($this->filepath, $data);

	}

	private function load()
	{
		if ($this->data !== FALSE) {
			return;
		}

		$data = @file_get_contents($this->filepath);
		if($data) {
			$this->data = unserialize($data);
		} else {
			$this->data = [];
		}
	}
}