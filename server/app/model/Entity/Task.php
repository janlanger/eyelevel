<?php


namespace App\Model\Task;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Kdyby\Doctrine\Entities\BaseEntity;

/**
 * Class Task
 * @package App\Model\Task
 *
 * @ORM\Entity()
 * @ORM\Table()
 */
class Task extends BaseEntity implements \JsonSerializable
{
	use Identifier;

	/**
	 * @ORM\Column
	 * @var
	 */
	protected $name;

	/**
	 * @ORM\Column(type="datetime")
	 * @var \DateTime
	 */
	protected $created;

	/**
	 * @ORM\Column(type="integer", nullable=true)
	 * @var int
	 */
	protected $duration;


	/**
	 * @ORM\Column(type="datetime", nullable=true)
	 * @var int
	 */
	protected $closed;

	function __construct()
	{
		$this->created = new \DateTime();
	}


	public function toArray()
	{
		$vars = get_object_vars($this);

		if(!empty($vars['created'])) {
			$vars['created'] = $vars['created']->format("Y-m-d H:i");
		}

		if(!empty($vars['closed'])) {
			$vars['closed'] = $vars['closed']->format("Y-m-d H:i");
		}
		return $vars;
	}

	function jsonSerialize()
	{
		return $this->toArray();
	}

	/**
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}


}