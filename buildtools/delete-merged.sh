#!/bin/bash
#
# Task delete merged local and remote branches
#

EXCLUDE="master|next|release"

# local
git branch --merged remotes/origin/next | grep -v -E "$EXCLUDE" | xargs -n 1 git branch -d

# remote
git branch -a --merged remotes/origin/next | grep -v -E "$EXCLUDE" | grep "$1" | cut -d "/" -f 3 | xargs -n 1 git push --delete "$1"
